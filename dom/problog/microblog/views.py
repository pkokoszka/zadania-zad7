# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.utils import timezone

from django import forms

from django.shortcuts import render
from django.http import HttpResponseRedirect

from models import *

from google.appengine.api import memcache

#forms
class AddPostForm(forms.Form):
    content = forms.CharField()

class LoginForm(forms.Form):
    login = forms.CharField(max_length=100)
    password = forms.CharField(max_length=32, widget=forms.PasswordInput)

class RegisterForm(forms.Form):
    login = forms.CharField(max_length=100)
    password = forms.CharField(max_length=32, widget=forms.PasswordInput)
    repassword = forms.CharField(max_length=32, widget=forms.PasswordInput)

class FilterForm(forms.Form):
    by_username = forms.CharField(max_length=100)




def AddPost(request):
    if not memcache.get('problog_dodaj'):
        formToRender = AddPostForm()

        logged = request.COOKIES['logged']
        username = request.COOKIES['username']

        strona =  render(request, 'microblog/AddPost.html', {
            'form': formToRender,
            'username': username,
            'logged': logged})

        memcache.add(key="problog_dodaj", value=strona, time=(60*5))
        return strona
    else:
        return memcache.get('problog_login')


def Login(request):
    if not memcache.get('problog_login'):
        formToRender = LoginForm()
        logged = request.COOKIES['logged']
        username = request.COOKIES['username']

        if logged == 'True':
            return HttpResponseRedirect(reverse('microblog.views.index'))


        strona = render(request, 'microblog/login.html', {
            'form': formToRender,
            'username': username,
            'logged': logged})

        memcache.add(key="problog_login", value=strona, time=(60*5))
        return strona
    else:
        return memcache.get('problog_login')

def Register(request):
    formToRender = RegisterForm()
    logged = request.COOKIES['logged']
    username = request.COOKIES['username']
    return render(request, 'microblog/register.html', {
        'form': formToRender,
        'username': username,
        'logged': logged})

def Filter(request):
    form = FilterForm()
    logged = request.COOKIES['logged']
    username = request.COOKIES['username']
    return render(request, 'microblog/filter.html', {
        'form': form,
        'username': username,
        'logged': logged})



def LoginPOST(request):
    form = LoginForm(request.POST)

    if request.method == 'POST' and form.is_valid():
        username = form.cleaned_data['login']
        password = form.cleaned_data['password']



        count = User.all().filter('login = ', username).filter('password = ', password).count()
        if count is 1:
            response = HttpResponseRedirect(reverse('microblog.views.komunikat', args=('ok', 'login_ok',)));
            response.set_cookie('logged', 'True')
            response.set_cookie('username', username)
            loginuser(username, password)
            return response
        else:
            response = HttpResponseRedirect(reverse('microblog.views.komunikat', args=('fail','DoesNotExist',)))
            response.set_cookie('logged', 'False')
            response.set_cookie('username', '')
            return response
    else:
        if 'zalogowany' in request.COOKIES:
            zalogowany = request.COOKIES['logged']
            if zalogowany == 'True':
                return HttpResponseRedirect(reverse('microblog.views.Login'))
        else:
            return HttpResponseRedirect(reverse('microblog.views.index'))

def RegisterPOST(request):
    form = RegisterForm(request.POST)

    if request.method == 'POST' and form.is_valid():

            username = form.cleaned_data['login']
            password = form.cleaned_data['password']
            repassword = form.cleaned_data['repassword']

            count = User.all().filter('login = ', username).count()
            if count < 1:
                if not password == repassword:
                    return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('blad', 'pwd_not_match',)))
                u = User(login=username, password=password)
                u.put()

                response = HttpResponseRedirect(reverse('microblog.views.komunikat', args=('ok', 'register_ok',)))
                response.set_cookie('logged', 'False')
                response.set_cookie('username', u.login)
                return response
            else:
                return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('blad', 'user_exists',)))





    else:
        return HttpResponseRedirect(reverse('microblog.views.Register'))


def komunikat(request, typ, message):
    tresc = ''
    if message == 'login_ok':
        tresc = 'Zalogowano pomyslnie'
    elif message == 'logout_ok':
        tresc = 'Wylogowano pomyslnie'
    elif message == 'not_logged_in':
        tresc = 'Nie jestes zalogowany'
    elif message == 'wpis_ok':
        tresc = 'Twoj wpis dodano pomyslnie'
    elif message == 'user_exists':
        tresc = 'Taki uzytkownik juz istnieje'
    elif message == 'pwd_not_match':
        tresc = 'Podane hasla nie sa identyczne'
    elif message == 'register_ok':
        tresc = 'Rejestracja poprawna. Mozesz sie zalogowac'
    else:
        tresc = 'Blad logowania'



    if typ == 'ok' or typ == 'blad':
        return render(request, 'microblog/komunikat.html',
            {
                'typ': typ,
                'tresc': tresc,
                'username': request.COOKIES['username'],
                'logged': request.COOKIES['logged'],
            })
    else:
        return render(request, 'microblog/komunikat.html',
            {
                'typ': 'blad',
                'tresc': tresc,
                'username': request.COOKIES['username'],
                'logged': request.COOKIES['logged'],
            })



def index(request):
    zalogowany = ''
    username =''

    if not 'logged' in request.COOKIES:
        zalogowany = 'False'
    if not 'username' in request.COOKIES:
        username = 'False'

    response = render(request, 'microblog/index.html', {
        'allPosts': Post.all().order('-data'),
    })
    response.set_cookie('logged', zalogowany)
    response.set_cookie('username', username)

    return response


def Logout(request):
    if request.COOKIES['logged'] == 'True':
        response = HttpResponseRedirect(reverse('microblog.views.komunikat', args=('ok', 'logout_ok',)))
        response.set_cookie('logged', 'false')
        response.delete_cookie('username')
        return response

    return HttpResponseRedirect(reverse('microblog.views.login'))


def AddPostPOST(request):
    if request.COOKIES['logged'] == 'True':
        form = AddPostForm(request.POST)
        if request.method == 'POST' and form.is_valid():
            user = request.COOKIES['username']
            tresc = form.cleaned_data['content']
            data = timezone.now()
            postToAdd = Post(author=user, content=tresc, data=data)
            postToAdd.put()
            return HttpResponseRedirect(reverse('microblog.views.index'))

        return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('blad', 'fdsfds',)))
    else:
        return HttpResponseRedirect(reverse('microblog.views.login'))



def FilterPOST(request):
    form = FilterForm(request.POST)
    if request.method == 'POST' and form.is_valid():
        user = form.cleaned_data['by_username']
        return HttpResponseRedirect(reverse('microblog.views.showUserPosts', args=(user,)))
    else:
        return HttpResponseRedirect(reverse('microblog.views.index'))

def showUserPosts(request, user):
    return render(request, 'microblog/index.html', {
        'logged': request.COOKIES['logged'],
        'username': request.COOKIES['username'],
        'allPosts': Post.all().filter('author = ', user).order('-data'),
        })



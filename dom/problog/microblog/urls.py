from django.conf.urls import patterns, url

import views


urlpatterns = patterns('',
        url(r'^$', views.index),
        url(r'^dodaj/', views.AddPost),
        url(r'^add/', views.AddPostPOST, name='add'),
        url(r'^login/', views.Login, name='login'),
        url(r'^doLogin/', views.LoginPOST, name='doLogin'),
        url(r'^logout/', views.Logout, name='logout'),
        url(r'^register/', views.Register, name='register'),
        url(r'^doRegister/', views.RegisterPOST, name='doRegister'),
        url(r'^komunikat/(?P<typ>\w+)/(?P<message>\w+)/$', views.komunikat, name='komunikat'),
        url(r'^filter/', views.Filter, name='filter'),
        url(r'^doFilter/', views.FilterPOST, name='doFilter'),
        url(r'^showuserposts/(?P<user>\w+)/', views.showUserPosts, name='showUserPosts'),
)
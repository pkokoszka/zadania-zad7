##from django.db import models

from google.appengine.ext import db as models

class Post(models.Model):
    author = models.StringProperty()
    content = models.StringProperty()
    data = models.DateTimeProperty(auto_now_add = 1)

    def __unicode__(self):
        return unicode.format(u'{0} {1}: {2}', self.data, self.author, self.content)

class User(models.Model):
    login = models.StringProperty()
    password = models.StringProperty()

    def __unicode__(self):
        return unicode.format(u'{0} {1}', self.login, self.password)

def loginuser(login, haslo):
    u = User.all().filter('login = ', login).filter('password = ', haslo)
    if not u:
        return False
    else:
        u.loggedin = True
        return True

def logoutuser(login):
    u = User.all().filter('login = ', login)
    u.loggedin = False

def is_authenticated(login):
    u = User.all().filter('login = ', login)
    return u.loggedin



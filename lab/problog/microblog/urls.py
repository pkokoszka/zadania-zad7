from django.conf.urls import patterns, url

import views


urlpatterns = patterns('',
        url(r'^$', views.index),
        url(r'^dodaj/', views.AddPost),
        url(r'^add/', views.AddPostPOST, name='add'),
        url(r'^komunikat/(?P<typ>\w+)/(?P<message>\w+)/$', views.komunikat, name='komunikat'),

)
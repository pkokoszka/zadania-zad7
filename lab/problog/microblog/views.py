# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.utils import timezone

from django import forms

from django.shortcuts import render
from django.http import HttpResponseRedirect

from models import *

from google.appengine.api import memcache

#forms
class AddPostForm(forms.Form):
    login = forms.CharField()
    content = forms.CharField()




def AddPost(request):

        formToRender = AddPostForm()

        strona =  render(request, 'microblog/AddPost.html', {
            'form': formToRender,
            })


        return strona





def komunikat(request, typ, message):
    tresc = ''
    if message == 'POSTOK':
        tresc = 'Twoj POST dodano pomyslnie'
    else:
        tresc = 'Blad'



    if typ == 'ok' or typ == 'blad':
        return render(request, 'microblog/komunikat.html',
            {
                'typ': typ,
                'tresc': tresc,
            })
    else:
        return render(request, 'microblog/komunikat.html',
            {
                'typ': 'blad',
                'tresc': tresc,
            })



def index(request):


    response = render(request, 'microblog/index.html', {
        'allPosts': Post.all().order('-data'),
    })

    return response




def AddPostPOST(request):
        form = AddPostForm(request.POST)
        if request.method == 'POST' and form.is_valid():
            login = form.cleaned_data['login']
            tresc = form.cleaned_data['content']
            data = timezone.now()
            postToAdd = Post(author=login, content=tresc, data=data)
            postToAdd.put()
            return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('ok', 'POSTOK',)))

        return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('blad', 'Wystapil blad',)))




